# COMP.SEC.300-2021-2022-1 Exercise Work Project

## Introduction

Patient video recordings represent sensitive health information.
Neuro Event Labs (NEL) is a health technology company specializing in the collection and analysis of patient video recordings.
As part of its scheme to protect them from data breaches, NEL uses a custom build of the GStreamer [matroska](https://gstreamer.freedesktop.org/documentation/matroska/index.html?gi-language=c) plugin to add [WebM Encryption Extensions](https://www.webmproject.org/docs/webm-encryption/).
The goal of this project is to access the security of this project and improve the sustainability of this patch set.

## Background

GStreamer is a popular and diverse cross-platform multimedia framework.
It forms an integral component of most desktop and many embedded Linux environments.
NEL uses GStreamer to collect, store, and process patient video recordings for the detection of epilepsy.
As these recordings contain highly sensitive data, they contain an additional layer of encryption via the WebM encryption scheme.
This scheme has wide browser support, as long as the file format conforms to the specification.

The challenge for NEL is that no native encryption support yet exists in the `matroskamux` element.
This is why a relatively small [patch set was written against GStreamer 1.16](https://gitlab.com/neuroeventlabs/gst-plugins-good).
While GStreamer 1.16 is essentially a long-term support release of the framework, there have been two stable versions (1.18 and 1.20) released to date.
Therefore, it is important that NEL find a sustainable path forward to bring all or some of the support upstream.

## Goals

The project has a number of goals, with the following questions supporting these goals.

- Assess the patch state against GStreamer 1.20:
-  Does the patch still apply?
-  Can any of the patch be accepted upstream by the GStreamer project?

- Assess the new `aesenc` element in GStreamer 1.20:
-  Can it be used to handle the encryption portion of the patch, instead of integrating support in `matroskamux`?

- Assess the security of the new implementation:
-  Are there insights from static analysis tools?
-  Can fuzzers find errors/crashing cases in the implementation?
-  Can the cipher strength be increased and still support in-browser playback?

By assessing these goals, the security posture and long-term viability of this encryption solution can be evaluated.
