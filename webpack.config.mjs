import { AngularWebpackPlugin } from '@ngtools/webpack';
import { fileURLToPath } from 'url';
import * as path from 'path';
import DefinePlugin from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import linkerPlugin from '@angular/compiler-cli/linker/babel';
import webpack from 'webpack';

const baseDir = path.dirname(fileURLToPath(import.meta.url));
export default {
  mode: 'development',
  entry: './src/main.ts',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [linkerPlugin],
            compact: false,
            cacheDirectory: true,
          }
        }
      },
      {
        test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
        loader: '@ngtools/webpack',
        exclude: [
          path.resolve(baseDir, 'node_modules'),
          path.resolve(baseDir, 'trials'),
        ],
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        exclude: [
          path.resolve(baseDir, 'node_modules'),
          path.resolve(baseDir, 'trials'),
        ],
      },
      {
        test: /\.css$/,
        loader: 'raw-loader',
        exclude: [
          path.resolve(baseDir, 'node_modules'),
          path.resolve(baseDir, 'trials'),
        ],
      },
    ],
  },
  output: {
    path: path.resolve(baseDir, 'dist'),
    filename: 'workbench.bundle.js',
  },
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
      process: 'process/browser',
    }),
    new webpack.DefinePlugin({
      PRODUCTION: false,
      SITES: JSON.stringify((process.env.SITE || '').split(',')), // ### get default from trial
      TRIAL: JSON.stringify(process.env.TRIAL),
    }),
    new AngularWebpackPlugin({
      tsconfig: 'tsconfig.json',
    }),
    new HtmlWebpackPlugin({
      chunks: ['main'],
      template: 'src/index.html',
    }),
  ],
  resolve: {
    extensions: ['.ts', '.js'],
    fallback: {
      stream: 'stream-browserify',
    },
  },
  devServer: {
    static: {
      directory: 'static',
      watch: false,
    },
  },
};
