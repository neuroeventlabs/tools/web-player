const webpack = require('webpack');

module.exports = {
  stories: [
    '../src/**/*.stories.mdx',
    '../src/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
  ],
  framework: '@storybook/angular',
  core: {
    builder: '@storybook/builder-webpack5',
  },
  staticDirs: [
    '../static',
  ],
  webpackFinal: async (config, { configType }) => {
    // Add missing polyfills here
    config.plugins.push(new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }));
    config.resolve.fallback = {
      stream: 'stream-browserify',
    };
    return config;
  },
}
