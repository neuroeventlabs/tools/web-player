# web-player

This project aims to provide a flexible Angular web player component oriented for media created with GStreamer.
It includes a [Storybook](https://storybook.js.org/) of cases demonstrating encrypted media playback in the web browser.

The project is published via GitLab Pages [here](https://neuroeventlabs.gitlab.io/tools/web-player).

## Install dependencies

The application requires [NodeJS](https://nodejs.org/en/download/package-manager/) and [Yarn](https://yarnpkg.com/getting-started/install) to be installed.

To install application dependencies, simply run:

    yarn

## Generate GStreamer test files

To generate the test files used by the application stories, run:

    ./tools/generate-webm-files.sh

The required GStreamer dependencies can be found in the [CI Dockerfile](tools/.gitlab.yml).

## Run the storybook project locally

Install the dependencies, then run:

    yarn storybook

If all goes well, the local application will automatically start in your browser.

## Build the storybook project for publishing

Run:

    yarn build-storybook

The built files are then available in the `storybook-static` directory.
If [Caddy](https://caddyserver.com/) installed, simply run `caddy run` to spin up a local production server at `https://localhost:8443`.
