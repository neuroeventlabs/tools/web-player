# This container contains everything needed to build the application and its test files for GitLab pages deployment.
# Build with: podman build -t registry.gitlab.com/neuroeventlabs/tools/web-player/ci -f ci.Dockerfile
# Push with: podman push registry.gitlab.com/neuroeventlabs/tools/web-player/ci
# Pull with: podman pull registry.gitlab.com/neuroeventlabs/tools/web-player/ci

FROM registry.fedoraproject.org/fedora-minimal:36

# Install nodejs, yarn, GStreamer development packages, compilers, etc.
RUN microdnf update -y \
  && microdnf install -y \
    gcc \
    git \
    gstreamer1-devel \
    gstreamer1-plugins-bad-free-devel \
    gstreamer1-plugins-base-devel \
    gstreamer1-plugins-good \
    meson \
    nodejs \
    openssl-devel \
    yarnpkg \
  && microdnf clean all

# Build the patched matroskamux element
RUN cd /tmp && git clone https://gitlab.com/neuroeventlabs/gst-plugins-good.git -b 1.16.2-encryption --depth=1 \
  && cd gst-plugins-good && meson -Dauto_features=disabled -Dmatroska=enabled builddir \
  && cd builddir && meson compile && meson install \
  && cd /tmp && rm -rf gst-plugins-good

RUN useradd user -m
USER user
ENV XDG_RUNTIME_DIR=/run/user/1000
WORKDIR /home/user
