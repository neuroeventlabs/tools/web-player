#!/usr/bin/env bash
set -euo pipefail

# Linter settings:
# shfmt -d -ln bash -i 2 -ci

log() {
  echo "${@}" >&2
}

dir=$(realpath "$(dirname "$(realpath "${0}")")/../static")
mkdir -p "${dir}"

if [ -f "${dir}/unencrypted.webm" ]; then
  log 'Unencrypted file already available; skipping.'
else
  log 'Generating unencrypted video...'
  gst-launch-1.0 videotestsrc num-buffers=300 ! video/x-raw,width=720,height=360 ! vp9enc \
    ! webmmux ! "file://${dir}/unencrypted.webm"
fi
log ''

if [ -f "${dir}/aesenc_encrypted.webm" ]; then
  log 'File encrypted with aesenc already available; skipping.'
else
  log 'Generating aesenc-encrypted video...'
  key='c37c1864a0eeb9a80d5152a14374c639'        # Note that the key is hard-coded for this example. Don't use in production!
  iv=$(openssl rand -rand /dev/urandom -hex 16) # Generate a random IV
  gst-launch-1.0 "file://${dir}/unencrypted.webm" ! matroskademux ! aesenc key="${key}" iv="${iv}" \
    ! taginject tags="license=(string)${iv}" ! webmmux ! "file://${dir}/aesenc_encrypted.webm"
fi
log ''

if [ -f "${dir}/matroskamux_encrypted.webm" ]; then
  log 'File encrypted with matroskamux already available; skipping.'
else
  log 'Generating matroskamux-encrypted video...'
  if ! gst-inspect-1.0 matroskamux | grep keyid >/dev/null; then
    log 'Your GStreamer installation appears to be missing encryption support on the matroskamux element.'
    log 'Please build it from https://gitlab.com/neuroeventlabs/gst-plugins-good'
    exit 1
  fi
  id='2ce07a3566d860c9b1d9e8294437d171'  # Will appear in the ContentEncKeyID field
  key='610849766439b3c65cb1d19178fd0e62' # Note that the key is hard-coded for this example. Don't use in production!
  gst-launch-1.0 "file://${dir}/unencrypted.webm" ! matroskademux \
    ! webmmux keyid="id:${id}" key="key:${key}" ! "file://${dir}/matroskamux_encrypted.webm"
fi
log ''
