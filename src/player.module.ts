import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PlayerComponent } from './player.component';

@NgModule({
  declarations: [
    PlayerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [PlayerComponent],
})
export class PlayerModule {
  public constructor() {
  }
}
