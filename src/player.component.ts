import { Buffer } from 'buffer';
import { Component, ChangeDetectionStrategy, ElementRef, HostListener, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SimpleBlock, EbmlTagFactory, EbmlStreamEncoder, EbmlStreamDecoder, EbmlDataTag, EbmlMasterTag, EbmlTag, EbmlTagId, EbmlTagPosition } from 'ebml-stream';

const CHUNK_SIZE: 10000000 = 10000000; // ~10MB download chunks

export enum EncryptionMode {
  WebM,
  AesEnc,
}

interface JWT {
  kty: string;
  alg: string;
  k: string;
  kid: string;
}

enum TranscodeState {
  None,
  InProgress,
  Queued,
  Finished,
  Error,
}

@Component({
  selector: 'player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayerComponent implements OnInit {
  private _video!: HTMLVideoElement;
  @ViewChild('video', { static: true }) protected set _videoRef(_video: ElementRef<HTMLVideoElement>) {
    this._video = _video.nativeElement;
    this._video.addEventListener('encrypted', this.onEncrypted.bind(this));
    this._video.addEventListener('error', this.onError.bind(this));
    this._video.addEventListener('canplay', this.onCanPlay.bind(this));
    this._video.addEventListener('canplaythrough', this.onCanPlayThrough.bind(this));
    this._video.addEventListener('durationchange', this.onDurationChange.bind(this));
    this._video.addEventListener('emptied', this.onEmptied.bind(this));
    this._video.addEventListener('keystatuschange', this.onKeyStatusChange.bind(this));
    this._video.addEventListener('loadeddata', this.onLoadedData.bind(this));
    this._video.addEventListener('loadedmetadata', this.onLoadedMetadata.bind(this));
    this._video.addEventListener('loadstart', this.onLoadStart.bind(this));
    this._video.addEventListener('playing', this.onPlaying.bind(this));
    this._video.addEventListener('play', this.onPlay.bind(this));
    this._video.addEventListener('progress', this.onProgress.bind(this));
    this._video.addEventListener('stalled', this.onStalled.bind(this));
    this._video.addEventListener('seeked', this.onSeeked.bind(this));
    this._video.addEventListener('seeking', this.onSeeking.bind(this));
    this._video.addEventListener('suspend', this.onSuspend.bind(this));
    this._video.addEventListener('timeupdate', this.onTimeUpdate.bind(this));
    this._video.addEventListener('volumechange', this.onVolumeChange.bind(this));
    this._video.addEventListener('waiting', this.onWaiting.bind(this));
    this._video.addEventListener('waitingforkey', this.onWaitingForKey.bind(this));
  }

  @ViewChild('progress', { static: true }) private _progress!: ElementRef<HTMLProgressElement>;
  //private _video!: HTMLVideoElement;
  private _mediaSource: MediaSource = new MediaSource();
  private _session: MediaKeySession | undefined;

  private _decoder!: EbmlStreamDecoder;
  private _encoder!: EbmlStreamEncoder;

  private log(...args: any[]): void {
    console.info(...args);
  }

  private error(...args: any[]): void {
    console.error(...args);
    this._errorMessage = args[0];
    this._changeDetector.markForCheck();
  }

  private hexToBase64UrlEncoded(key: string): string {
    return Buffer.from(key, 'hex').toString('base64').replace(/\+/g, '-').replace(/\//g, '_').replace(/=*$/, '');
  }

  private _transcodeState: TranscodeState = TranscodeState.None;
  private _transcodeResolve: (() => void) | undefined;
  private _transcodeReject: ((error: Error) => void) | undefined;

  private _transcodeHeaders: Buffer[] = [];
  private _transcodeClusters: Buffer[] = [];
  private _encryptedClusters: EbmlMasterTag[] = [];

  private _tags: EbmlTag[] = [];

  // Loads the buffers into the transcoder and waits for all data to be processed.
  // The caller can then read the transcoded clusters accordingly.
  private async enqueue(buffer: Buffer): Promise<void> {
    if (this._transcodeState === TranscodeState.InProgress || this._transcodeResolve) {
      throw new Error('enqueue() is running; please wait until its previous invocation completes.');
    }

    if (!(this._transcodeState === TranscodeState.None || this._transcodeState === TranscodeState.Queued)) {
      throw new Error(`enqueue() is in an unexpected state: ${this._transcodeState}`);
    }

    return new Promise((resolve: () => void, reject: (error: Error) => void) => {
      this._transcodeState = TranscodeState.InProgress;
      this._transcodeResolve = resolve;
      this._transcodeReject = reject;
      this._decoder.write(buffer, async (): Promise<void> => {
        if (this._transcodeState !== TranscodeState.InProgress) {
          return
        }

        if (this._encryptedClusters.length) {
          try {
            await this.decryptClusters();
          } catch (error) {
            this.error('Unable to decrypt clusters:', error);
            this._transcodeState = TranscodeState.Error;
            this._transcodeReject!(error as Error);
            this._transcodeResolve = this._transcodeReject = undefined;
            return;
          }
        }

        this._transcodeState = TranscodeState.Queued;
        this._transcodeResolve!();
        this._transcodeResolve = this._transcodeReject = undefined;
      });
    });
  }

  // This method is currently unused, but it demonstrates how to inject an encryption context into an otherwise normal file
  protected injectEncryptionHeader(tag: EbmlTag): void {
    // This tag injection works, but it requires that the data is encrypted in CTR mode for playback.
    if (tag.id === EbmlTagId.Tracks) {
      const tracks: EbmlMasterTag[] = (tag as EbmlMasterTag).Children as EbmlMasterTag[];
      for (const track of tracks) {
        if (track.Children.some((t: EbmlTag) => t.id === EbmlTagId.ContentEncodings)) {
          // The track already has content encodings; skip
          continue;
        }

        // Arranged in blocks to make the hierarchy clear
        const contentEncodings: EbmlMasterTag = EbmlTagFactory.create(EbmlTagId.ContentEncodings);
        {
          const contentEncoding: EbmlMasterTag = EbmlTagFactory.create(EbmlTagId.ContentEncoding);
          {
            const contentEncodingOrder: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.ContentEncodingOrder);
            contentEncodingOrder.data = 0; // Count from 0
            const contentEncodingScope: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.ContentEncodingScope);
            contentEncodingScope.data = 1; // All frame contents
            const contentEncodingType: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.ContentEncodingType);
            contentEncodingType.data = 1; // Encryption
            const contentEncryption: EbmlMasterTag = EbmlTagFactory.create(EbmlTagId.ContentEncryption);
            {
              const contentEncAlgo: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.ContentEncAlgo);
              contentEncAlgo.data = 5; // AES
              const contentEncKeyID: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.ContentEncKeyID);
              contentEncKeyID.data = Buffer.from('3606de11c26a10a2edfebbcedd428d16', 'hex');
              const contentEncAESSettings: EbmlMasterTag = EbmlTagFactory.create(EbmlTagId.ContentEncAESSettings);
              {
                const aesSettingsCipherMode: EbmlDataTag = EbmlTagFactory.create(EbmlTagId.AESSettingsCipherMode);
                aesSettingsCipherMode.data = 1; // CTR
                contentEncAESSettings.Children.push(aesSettingsCipherMode);
              }
              contentEncryption.Children.push(contentEncAlgo, contentEncKeyID, contentEncAESSettings);
            }
            contentEncoding.Children.push(contentEncodingOrder, contentEncodingScope, contentEncodingType, contentEncryption);
          }
          contentEncodings.Children.push(contentEncoding);
        }
        track.Children.push(contentEncodings);
      }
    }
  }

  // This method performs decryption of a chain of clusters like those produced by aesenc.
  private async decryptClusters(): Promise<void> {
    // Load all keys provided by the runtime (TODO: this could be done immediately when the keys are set.)
    const keys: Record<string, CryptoKey> = {};
    for (const [key, value] of Object.entries(this._keys)) {
      keys[key] = await window.crypto.subtle.importKey(
        'raw', Buffer.from(value, 'hex'), { name: 'AES-CBC', length: 128 }, false, ['decrypt']);
    }

    // Decrypt
    if (!this._iv) { // TODO: read IV per-track, not per-file
      throw new Error('No IV set for this track.');
    }
    let iv: Buffer = Buffer.from(this._iv, 'hex'); // TODO: support decrypting from anywhere, not necessarily the first buffer
    for (const cluster of this._encryptedClusters) {
      for (const child of cluster.Children) {
        if (child.id !== EbmlTagId.SimpleBlock) {
          continue;
        }
        const block: SimpleBlock = child as SimpleBlock;
        const key: CryptoKey = keys[block.track]; // TODO: use track UID for key ID instead
        if (!key) {
          throw new Error(`No key found for track: ${block.track}`);
        }
        const payload: ArrayBuffer = await window.crypto.subtle.decrypt(
          { name: 'AES-CBC', length: 128, iv }, key, block.payload);
          iv = block.payload.slice(-16);
          block.payload = Buffer.from(payload);
          block.size = block.payload.byteLength + 4;
      }
      this._tags.push(cluster);
      this._encoder.write(cluster);
    }
  }

  private async onEncoderData(data: Uint8Array): Promise<void> {
    const tag: EbmlTag | undefined = this._tags.shift();
    switch (tag?.id) {
      case EbmlTagId.Segment:
        if (tag.position === EbmlTagPosition.End) {
          // TODO: where do we put this tag; anywhere?
          break;
        }
        // fall through
      case EbmlTagId.EBML:
      case EbmlTagId.SeekHead:
      case EbmlTagId.Info:
      case EbmlTagId.Tracks:
      case EbmlTagId.Cues:
      case EbmlTagId.Tags:
        this._transcodeHeaders.push(Buffer.from(data));
        break;
      case EbmlTagId.Cluster:
        this._transcodeClusters.push(Buffer.from(data));
        break;
      default:
        this.error('Unexpected tag in stream:', tag);
        break;
    }
  }

  private onEncoderError(error: Error): void {
    this._transcodeState = TranscodeState.Error;
    this.error(error);
    this._transcodeReject!(error);
    this._transcodeResolve = this._transcodeReject = undefined;
  }

  private onEncoderFinish(): void {
    this._transcodeState = TranscodeState.Finished;
  }

  private async onDecoderData(tag: EbmlTag): Promise<void> {
    if (this._encryptionMode === EncryptionMode.AesEnc) {
      // Defer cluster decryption
      if (tag.id === EbmlTagId.Cluster) {
        this._encryptedClusters.push(tag as EbmlMasterTag);
        return;
      }

      // Check for the IV in the tags (TODO: generically parse tags for a more sane lookup)
      if (tag.id === EbmlTagId.Tags) {
        let ivTag: EbmlDataTag | undefined;
        (tag as EbmlMasterTag).Children.some((t: EbmlTag) => {
          if (t.id !== EbmlTagId.Tag) {
            return;
          }
          for (const child of (t as EbmlMasterTag).Children as EbmlMasterTag[]) {
            if (child.id !== EbmlTagId.SimpleTag) {
              continue;
            }
            const tagName: string = (child.Children.find((t: EbmlTag) => t.id === EbmlTagId.TagName) as EbmlDataTag)?.data;
            if (tagName !== 'TERMS_OF_USE\u0000') {
              continue;
            }
            ivTag = child.Children.find((t: EbmlTag) => t.id === EbmlTagId.TagString) as EbmlDataTag;
            return ivTag;
          }
        });
        this._iv = ivTag?.data?.slice(0, -1);
      }
    }

    this._tags.push(tag);
    this._encoder.writeTag(tag);
  }

  private onDecoderError(error: Error): void {
    this._transcodeState = TranscodeState.Error;
    this.error(error);
    this._transcodeReject!(error);
    this._transcodeResolve = this._transcodeReject = undefined;
  }

  private onDecoderFinish(): void {
    this._transcodeState = TranscodeState.Finished;
    this._encoder.end();
  }

  private async resetTranscodeState(): Promise<void> {
    if (this._encoder) {
      this._encoder.removeAllListeners('data');
      this._encoder.removeAllListeners('error');
      this._encoder.removeAllListeners('finish');
    }

    if (this._decoder) {
      this._decoder.removeAllListeners('data');
      this._decoder.removeAllListeners('error');
      this._decoder.removeAllListeners('finish');
    }

    this._encoder = new EbmlStreamEncoder();
    this._encoder.on('data', (chunk) => this.onEncoderData(chunk));
    this._encoder.on('error', (error: Error) => this.onEncoderError(error));
    this._encoder.on('finish', () => this.onEncoderFinish());

    this._decoder = new EbmlStreamDecoder({
      bufferTagIds: [
        EbmlTagId.Cluster,
        EbmlTagId.Cues,
        EbmlTagId.EBML,
        EbmlTagId.Info,
        EbmlTagId.SeekHead,
        EbmlTagId.Tags,
        EbmlTagId.Tracks,
      ],
    });
    this._decoder.on('data', (tag: EbmlTag) => this.onDecoderData(tag));
    this._decoder.on('error', (error: Error) => this.onDecoderError(error));
    this._decoder.on('finish', () => this.onDecoderFinish());

    this._transcodeHeaders = [];
    this._transcodeClusters = [];
    this._encryptedClusters = [];
    this._tags = [];
  }

  // This models appendBufferAsync (https://developer.mozilla.org/en-US/docs/Web/API/SourceBuffer/appendBufferAsync)
  private async appendBuffers(buffers: Buffer[]): Promise<SourceBuffer> {
    return new Promise((resolve: (buffer: SourceBuffer) => void, reject: (error: Event | Error) => void) => {
      try {
        const sourceBuffer: SourceBuffer = this._mediaSource.addSourceBuffer('video/webm');
        sourceBuffer.addEventListener('updateend', () => {
          if (sourceBuffer.updating) {
            sourceBuffer.abort();
            reject(new Error('SourceBuffer is still updating.'));
          }
          if (buffers.length) {
            sourceBuffer.appendBuffer(buffers.shift()!);
            return;
          }
          resolve(sourceBuffer);
        });
        sourceBuffer.addEventListener('error', (error: Event) => {
          reject(error);
        });
        sourceBuffer.appendBuffer(buffers.shift()!);
      } catch (error) {
        reject(error as Error);
      }
    });
  }

  private async loadSrc(): Promise<void> {
    this._progressValue = 0;
    this._changeDetector.markForCheck();

    const config: MediaKeySystemConfiguration[] = [{
      initDataTypes: ['webm'],
      videoCapabilities: [{
        contentType: 'video/webm; codecs="vp9"',
      }],
      audioCapabilities: [{
        contentType: 'audio/webm; codecs="opus"',
      }],
    }];

    if (!this._video.mediaKeys) {
      try {
        const keySystemAccess: MediaKeySystemAccess = await window.navigator.requestMediaKeySystemAccess('org.w3.clearkey', config);
        const mediaKeys: MediaKeys = await keySystemAccess.createMediaKeys();
        await this._video.setMediaKeys(mediaKeys);
      } catch (error) {
        this.log('Failed to initialize media key access:', error);
        return;
      }
    }

    await this.resetTranscodeState();

    // 1. Fetch some amount of video in a loop (must not exceed DOM quota)
    let offset: number = 0;
    while (true) {
      const response: Response = await fetch(new Request(this._src!, {
        headers: {
          Range: `bytes=${offset}-${offset + CHUNK_SIZE - 1}`,
        },
      }));
      if (!response.ok) {
        this.log('Request failed:', response);
        return;
      }

      try {
        const buffer: Buffer = Buffer.from(await response.arrayBuffer());
        offset += buffer.byteLength;
        await this.enqueue(buffer);
      } catch (error) {
        this.error(error);
        return;
      }

      const total: number = Number.parseInt((response.headers.get('Content-Range') || '').split('/')[1]);
      this._progressValue = offset / total;
      this._changeDetector.markForCheck();

      // TODO: implement only downloading certain range (requires moving this block to a new function for downloading range)

      // No need to fetch more, as we have reached the end of the file
      if (response.status === 200 || offset === total) {
        break;
      }
    }

    this._progressValue = 1;
    this._changeDetector.markForCheck();

    // 2. Call addSourceBuffer('video/webm') in reasonably-sized chunks
    // TODO: separate this into its own function for swapping out loaded ranges
    try {
      const sourceBuffer: SourceBuffer = await this.appendBuffers([Buffer.concat(this._transcodeHeaders), ...this._transcodeClusters]);
      if (sourceBuffer) {
        this.log('source buffer creation successful');
      }
    } catch (error) {
      this.error('Failed to append data into source buffer:', error);
      return;
    }
  }

  private async onEncrypted(): Promise<void> {
    const e: MediaEncryptedEvent = event as MediaEncryptedEvent;
    this.log('encrypted', e);
    if (!this._video.mediaKeys) {
      this.error('no media keys found');
      return;
    }

    this._session = this._video.mediaKeys.createSession();
    this._session.addEventListener('message', this.onMediaKeyMessage.bind(this), false);
    try {
      await this._session.generateRequest(e.initDataType, e.initData!);
    } catch(error) {
      this.error('failed to request license for protected media playback', error);
    }
  }

  private async onMediaKeyMessage(e: MediaKeyMessageEvent): Promise<void> {
    this.log('media key message', e.message);

    const request: { kids: string[], type: string } = JSON.parse(new TextDecoder().decode(e.message));
    const id: string = Buffer.from(request.kids[0], 'base64').toString('hex').slice(0, 32);
    if (!(id in this._keys)) {
      this.error(`No key found for id: ${id}`);
      await this._session?.close();
      return;
    }

    try {
      const jwt: JWT = { kty: 'oct', alg: 'A128KW', kid: request.kids[0], k: this.hexToBase64UrlEncoded(this._keys[id]) };
      const license: Uint8Array = (new TextEncoder()).encode(JSON.stringify({ keys: [jwt], type: request.type }));
      await this._session!.update(license);
    } catch (error) {
      this.error('Failed to update protected playback session.', error);
      await this._session?.close();
      return;
    }
  }

  private onError(): void {
    this.log('error', this._video.error);
  }

  private onCanPlay(): void {
    this.log('canplay');
  }

  private onCanPlayThrough(): void {
    this.log('canplaythrough');
  }

  private onDurationChange(): void {
    this.log('durationchange');
  }

  private onEmptied(): void {
    this.log('emptied');
  }

  private onKeyStatusChange(): void {
    this.log('keystatuschange');
  }

  private onLoadedData(): void {
    this.log('loadeddata');
  }

  private onLoadedMetadata(): void {
    this.log('loadedmetadata');
  }

  private onLoadStart(): void {
    this.log('loadstart');
  }

  private onPlaying(): void {
    this.log('playing');
  }

  private onPlay(): void {
    this.log('play');
  }

  private onProgress(): void {
    this.log('progress');
  }

  private onStalled(): void {
    this.log('stalled');
  }

  private onSeeked(): void {
    this.log('seeked');
  }

  private onSeeking(): void {
    this.log('seeking');
  }

  private onSuspend(): void {
    this.log('suspend');
  }

  private onTimeUpdate(): void {
    this.log('timeupdate');
  }

  private onVolumeChange(): void {
    this.log('volumechange');
  }

  private onWaiting(): void {
    this.log('waiting');
  }

  private onWaitingForKey(): void {
    this.log('waitingforkey');
  }

  public set controls(controls: boolean) {
    this._video.controls = controls;
  }

  private _src: string | undefined;
  public set src(src: string) {
    this.log('setting src:', src);
    this._src = src;
    this._video.src = URL.createObjectURL(this._mediaSource);
    this.loadSrc();
  }

  private _keys: Record<string, string> = {};
  public set keys(keys: Record<string, string>) {
    this.log('setting keys:', keys);
    this._keys = { ...keys };
  }

  private _iv: string | undefined;
  private _encryptionMode: EncryptionMode = EncryptionMode.WebM;
  public set encryptionMode(encryptionMode: EncryptionMode) {
    this._encryptionMode = encryptionMode;
  }

  private _progressValue: number = 0;
  public get progressValue(): number {
    return this._progressValue;
  }

  private _errorMessage: string | undefined;
  public get errorMessage(): string | undefined {
    return this._errorMessage;
  }

  public constructor(
    _element: ElementRef<HTMLVideoElement>,
    private _changeDetector: ChangeDetectorRef,
  ) {
  }

  public async ngOnInit(): Promise<void> {
  }
}
