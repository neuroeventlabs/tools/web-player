import 'zone.js/dist/zone';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { PlayerModule } from './player.module';

declare const PRODUCTION: boolean;

if (PRODUCTION) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(PlayerModule)
  .catch(err => console.error(err));
