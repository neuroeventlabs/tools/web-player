// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { EncryptionMode, PlayerComponent } from '../player.component';

export default {
  title: 'Project/Player',
  component: PlayerComponent,
  args: {
    controls: true,
  },
  parameters: {
    docs: {
      page: null,
    },
  },
} as Meta;

const Template: Story<PlayerComponent> = (args: PlayerComponent) => ({
  props: args,
});

export const Unencrypted = Template.bind({});
Unencrypted.args = {
  src: 'unencrypted.webm',
};

export const MatroskamuxEncrypted = Template.bind({});
MatroskamuxEncrypted.args = {
  src: 'matroskamux_encrypted.webm',
  encryptionMode: EncryptionMode.WebM,
  keys: {
    '32636530376133353636643836306339': '610849766439b3c65cb1d19178fd0e62',
  },
};

export const AesencEncrypted = Template.bind({});
AesencEncrypted.args = {
  src: 'aesenc_encrypted.webm',
  encryptionMode: EncryptionMode.AesEnc,
  keys: {
    '1': 'c37c1864a0eeb9a80d5152a14374c639',
  },
};
