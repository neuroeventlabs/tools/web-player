import { Meta } from '@storybook/addon-docs';

<Meta title="Web Player Encryption Storybook" />

# Web Player Encryption Storybook

This project demonstrates several "stories" using web standards to play content-protected (encrypted) media.
The samples here are focused around playing encrypted files created with [GStreamer](https://gstreamer.freedesktop.org/), particularly those with an out-of-the-box experience.
By exploring different techniques and use cases, a developer can opt for a configuration that balances the data protection threat model with the total cost of ownership.

This project demonstrates an "advanced" WebM video player with several file encryption options:

1. No encryption: the player functions fine when no encryption is present.
The file is generated with a standard GStreamer pipeline.

2. [Standard WebM encryption](https://www.webmproject.org/docs/webm-encryption/) using [W3C Clear Key](https://www.w3.org/TR/encrypted-media/#clear-key) system and a [patched](https://gitlab.com/neuroeventlabs/gst-plugins-good/-/commit/7b49786237ffbf24216761e7fa631519cb0b720e) GStreamer's [`matroskamux`](https://gstreamer.freedesktop.org/documentation/matroska/matroskamux.html) plugin.
Until these patches are available upstream, a developer would need to apply these patches to GStreamer to produce these types of files.

3. Non-standard WebM encryption supporting use of GStreamer's [`aesenc`](https://gstreamer.freedesktop.org/documentation/aes/aesenc.html) plugin.
This provides an out-of-the-box encryption experience using GStreamer, but the decryption pattern is non-standard and has some disadvantages, such as having to perform decryption within the application instead of a [content decryption module (CDM)](https://en.wikipedia.org/wiki/Encrypted_Media_Extensions#Content_Decryption_Modules).
Contrary to standard WebM encryption, this method allows increasing the key size to 256 bits.
With modifications to `aesenc`, this could be extended even further.

## What makes this player "advanced"?

The simplest web player can be build with the HTML [`<video>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video) element.
When working with encrypted videos, the [Encrypted Media Extensions (EME) API](https://developer.mozilla.org/en-US/docs/Web/API/Encrypted_Media_Extensions_API) and the [Media Source Extensions (MSE) API](https://developer.mozilla.org/en-US/docs/Web/API/Media_Source_Extensions_API) must be used.
Use of these APIs almost certainly leads to additional video file parsing to be done within the application, such as using the [`ebml-stream`](https://www.npmjs.com/package/ebml-stream) module as this project does.
Furthermore, achieving on-demand seeking also requires use of MSE.
All of these together greatly increase the complexity of the video player.

## With existing third-party video players available, why build a new one?

There are certainly existing libraries for integrating EME/MSE-powered video players into your application.
This project is about building a test environment for making it easier to develop one from scratch, while adding features incrementally and researching different encryption methods.
For example, the [`aesenc` encryption](?path=/story/project-player--unencrypted) example doesn't use MSE, but [WebCrypto](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API), to perform decryption.
This type of experiment shows how it is possible to access a much wider cross-section of available cryptographic ciphers and modes, as well as off-the-shelf general-purpose tools like `aesenc`.
However, this approach does not use a CDM, making the decrypted content accessible to the JavaScript context at runtime, which may be considered a greater content protection risk.
This may be of little significance when compared to the Clear Key approach, however, because the key is also available at runtime in that case.

## OK, tell me more!

Take a look at the examples above (or from the menu on the left) to get familiar with them.

As this project has just been created, here's a laundry list of improvement ideas:

- [ ] Complete coverage for errors and state changes; the player should be exception-safe.
- [ ] Complete logging framework so that log messages are configurable/filterable.
- [ ] Patch GStreamer's `aesenc` to support more cipher modes; it isn't far from being able to supporting CTR modes used in standard WebM encryption.
- [ ] Add an example of WebM encryption header injection, allowing the client to initiate a key session out-of-band.
- [ ] Better separate the concerns of the monolithic `Player` class into separate classes: general playback and interaction, fetching/buffering, parsing ("transcoding"), decryption.
- [ ] Finish support for fetching/seeking (write now, the whole file is downloaded, even if done in chunks).
- [ ] Add support for dropping/replacing ranges so that only some video buffers in memory.
- [ ] Add more advanced examples, such as multiple encryption envelopes.
- [ ] Add a code linter.
- [ ] Style Storybook to look prettier. Video UIs should be dark, right?
